```{r parameters-and-defaults, include = FALSE}
module <- "scRNAseq"
section <- "dimension_reduction"
```

```{r parameter-merge, include = FALSE}
local_params <- module %>%
  options() %>%
  magrittr::extract2(module) %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```

```{r scRNAseq-dimension-reduction-D-isomds-checks, include = FALSE}
assertive.sets::assert_is_subset(local_params$features, colnames(SummarizedExperiment::colData(object_filtered)))
```

### Non-metric multidimensional scaling

**N**on-metric **m**ulti**d**imensional **s**caling [@kenkel_nmds_1986] (NMDS) attemps to represent the pairwise dissimilarity between samples in a low-dimensional space, unlike other methods which try to maximise correspondence between samples. It is a rank-based approach, meaning that original distance between samples is substituted with ranks[^3]. While information about the magnitude of distances is lost, rank-based methods are generally more robust to data which do not have an identifiable distribution.

[^3]: Ranking: Instead of numeric ordination (e.g. value-based *4.4 >> 3.1*), values are replaced with ranks, i.e. the highest value gets assigned the highest rank etc. (*rank 2 >> rank 1*). 

```{r scRNAseq-dimension-reduction-D-isomds-processing, echo=FALSE, include=FALSE}
features <- NULL
if (local_params$subset_het) {
  features <- SummarizedExperiment::rowData(object_filtered)$is_het
}

object_filtered <- singlecellutils::reduce_dimension(object_filtered, exprs_values = local_params$assay, flavor = "isomds", features = features, method = local_params$wgcna_cor_method, k = local_params$dims, maxit = local_params$maxiter, tol = 1e-4, p = 2)
```

```{r scRNAseq-dimension-reduction-D-isomds-figure-params, include = FALSE, echo = FALSE}
fig_height <- ReporteR.base::estimate_figure_height(
  height_in_panels = 1,
  panel_height_in_in = params$formatting_defaults$figures$panel_height_in,
  axis_space_in_in = params$formatting_defaults$figures$axis_space_in,
  mpf_row_space = as.numeric(grid::convertUnit(grid::unit(5, 'mm'), 'in')),
  max_height_in_in = params$formatting_defaults$figures$max_height_in)

caption <- glue::glue("Dimensionality reduction using **NMDS** [@kenkel_nmds_1986] on a dissimilarity structure (calculated as *1 - {local_params$wgcna_cor_method} correlation*), keeping {local_params$dims} dimensions (shown are the first two).")
```

```{r scRNAseq-dimension-reduction-D-isomds-figure, echo = FALSE, message=FALSE, warning=FALSE, fig.height = fig_height$global, fig.cap = caption}
figure_dimred_som_tsne <- multipanelfigure::multi_panel_figure(height = fig_height$sub, columns = min(3, length(local_params$features)), rows = 1, unit = "in")

for(i in 1:min(3, length(local_params$features))) {
  tmp_plot <- object_filtered %>%
    scater::plotReducedDim(use_dimred = "isomds", colour_by = local_params$features[i], add_ticks = FALSE) +
    ggplot2::guides(colour = FALSE) +
    theme_dimred_scatter
  figure_dimred_som_tsne <- multipanelfigure::fill_panel(figure_dimred_som_tsne, tmp_plot)
}

figure_dimred_som_tsne
```