```{r scRNAseq-infrastructural-setup, include = FALSE}
options(knitr.duplicate.label = "allow")
options(xtable.comment = FALSE)
libs <- c(
  "magrittr",
  "ReporteR.base",
  "ReporteR.scRNAseq"
)
for (lib in libs) {
  library(lib, character.only = TRUE, quietly = TRUE)
}

unlockBinding(sym = "params", env = environment())
params %<>%
  ReporteR.base::flag_persistent()
lockBinding(sym = "params", env = environment())
```


```{r scRNAseq-intro, echo = FALSE, child = system.file('content/01-intro.Rmd', package = 'ReporteR.scRNAseq', mustWork = TRUE), R.options = params, eval = TRUE}
```

```{r scRNAseq-quality-control, echo = FALSE, include = FALSE, R.options = params, eval = ifelse(exists('params'), 'quality_control' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '02-quality-control.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-quality-control-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '02-quality-control.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '02-quality-control.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-normalization, echo = FALSE, include=FALSE, R.options = params, eval = ifelse(exists('params'), 'normalization' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '03-normalization.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-normalization-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '03-normalization.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '03-normalization.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-batch-assessment, echo = FALSE, include=FALSE, R.options=params, eval = ifelse(exists('params'), 'batch_assessment' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '04-batch-assessment.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-batch-assessment-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '04-batch-assessment.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '04-batch-assessment.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-feature-selection, echo = FALSE, include = FALSE, R.options = params, eval = ifelse(exists('params'), 'feature_selection' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '04-feature-selection.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-feature-selection-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '04-feature-selection.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '04-feature-selection.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-dimension-reduction, echo = FALSE, include = FALSE, R.options = params, eval = ifelse(exists('params'), 'dimension_reduction' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '05-dimension-reduction.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-dimension-reduction-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '05-dimension-reduction.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '05-dimension-reduction.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-clustering, echo = FALSE, include = FALSE, R.options = params, eval = ifelse(exists('params'), 'clustering' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '06-clustering.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-clustering-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '06-clustering.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '06-clustering.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-markers, echo = FALSE, include = FALSE, R.options = params, eval = ifelse(exists('params'), 'marker_genes' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '07-marker.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-markers-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '07-marker.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '07-marker.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

```{r scRNAseq-differential-expression, echo = FALSE, include = FALSE, R.options = params, eval = ifelse(exists('params'), 'differential_expression' %in% names(params[['scRNAseq']]), FALSE)}
rmd_path <- system.file(file.path('content', '08-differential-expression.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)
md_path = ReporteR.base::make_md_path(rmd_path)
knitr::knit(rmd_path, output = md_path)
```

```{r scRNAseq-differential-expression-include, echo = FALSE, results = "asis", eval = assertive.files::is_readable_file(ReporteR.base::make_md_path(system.file(file.path('content', '08-differential-expression.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)))}
ReporteR.base::make_md_path(system.file(file.path('content', '08-differential-expression.Rmd'), package = 'ReporteR.scRNAseq', mustWork = TRUE)) %>%
  readLines() %>%
  cat(sep = '\n')
```

